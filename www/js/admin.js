const API_URL = 'http://212.195.183.176:1337/';
const GEOCODER_URL = 'https://api-adresse.data.gouv.fr/search/';


var listOfDomainForUpdate = [];

var locked = false;

$(document).ready( function () {
	//On récupère la liste des domaines
	getData();
});

function getData() {
	//On réinitialise la liste des domaines
	listOfDomainForUpdate = [];
	//Et on vide la datalist pour la recherche d'un domaine lors d'une modification ou d'une suppression
	$('datalist').empty();

	$.ajax({
		url: `${API_URL}domaine`,
		method: 'GET',
		dataType: 'json',
		success : (data) => {
			let listName = [];
			for (let i = 0; i < data.length; i++) {
				let d = data[i];
				if (d.domaine != undefined && d.adresse != undefined && d.appellations != undefined && !isNaN(d.longitude) && !isNaN(d.latitude))
					listOfDomainForUpdate.push(d);
			}
			//On trie la liste dans l'ordre alphabétique sur les noms de domaine
			listOfDomainForUpdate.sort(function (a, b) {
				var x = a.domaine.toUpperCase(), y = b.domaine.toUpperCase();
				return x < y ? -1 : x > y ? 1 : 0;
			});

			for (let i=0; i < listOfDomainForUpdate.length; i++) {
				let option = $('<option value="' + listOfDomainForUpdate[i]["domaine"] + '">');
				$('datalist').append(option);
			}
		}
	});
}

//Ajoute une ligne pour l'ajout d'une appellation
$('.addAOC').on("click", function () {
	let numberOfAoc = $(this).parent().parent().find('.input-aoc').length;
	numberOfAoc++;
	let aoc = $('<input>').attr('type', 'text').addClass('input-aoc').attr('data-id', numberOfAoc).attr('placeholder', 'Appellation ' + numberOfAoc).css('display', 'none');
	aoc.insertBefore($(this).parent()).show("fast");
});

$('.required').on("input", function () {
	$(this).parent().find('.error').hide("fast");
});

$('#add .submit').on("click", function () {
	let domaine = $('#add .input-domain').val().trim();
	let adresse =  $('#add .input-address').val().trim();
	let aoc = [];

	$('#add .input-aoc').each(function (index) {
		let appellation = $('#add .input-aoc[data-id="' + (index+1) + "\"").val().trim();
		if (appellation != "")
			aoc.push(appellation);
	});

	let formValid = true;
	$('#add .required').each(function () {
		if ($(this).val().trim() == '') {
			$(this).parent().find('.error').html('Ce champs est obligatoire.').show("fast");
			formValid = false;
		}
	});

	if (formValid) {
		if (!locked) {
			locked = true;
			$($('#add .submit')).addClass('disabled');
			//Requête vers le geocoder du gouvernement pour obtenir les cooronnées géographiques du domaine en fonction de son adresse
			$.ajax({
				url: GEOCODER_URL,
				method: 'GET',
				data: 'q=' + adresse.replace(/,/g, "+").replace(/ /g, "+") + '&limit=1',
				success: (data) => {
					if (data["features"].length != 0) {
						let longitude = data["features"][0]["geometry"]["coordinates"][0];
						let latitude = data["features"][0]["geometry"]["coordinates"][1];
						let domaineJSON = {
							"domaine": domaine,
							"adresse": adresse,
							"longitude": longitude,
							"latitude": latitude,
							"appellations": aoc
						}
						//On enregistre ensuite le domaine en passant par l'API
						$.ajax({
							url: `${API_URL}domaine`,
							method: "POST",
							dataType: 'json',
							contentType: 'application/json',
							data: JSON.stringify(domaineJSON),
							success: function (data) {
								$('#add .result').html("Le domaine " + data["domaine"] + " a bien été enregistré.").css("color", "#28A745");
								getData();
							},
							error: function (data) {
								$('#add .result').html("Une erreur s'est produite. Veuillez réessayer.").css("color", "#DC3545");
							}
						});
					} else {
						$('#add .result').html("Adresse non reconnue. Veuillez réessayer.").css("color", "#DC3545");
					}
				},
				error: function (data) {
					$('#add .result').html("Une erreur s'est produite. Veuillez réessayer.").css("color", "#DC3545");
				}
			});

			setTimeout(function () {
				locked = false;
				$('#add .submit').removeClass('disabled');
			}, 1000);

		}
	}
});

$('#input-domain-update').on("input", function () {
	let found = false;

	for (let i=0; i < listOfDomainForUpdate.length; i++) {
		d = listOfDomainForUpdate[i];

		//Si la saisie correspond au nom d'un domaine, ou qu'un domaine a été choisi dans la liste
		if (d.domaine == $(this).val()) {
			//On auto complète le formulaire de modification avec les infos du domaine
			$('#update .input-domain').prop("value", d.domaine).show("fast");
			$('#update .input-address').prop("value", d.adresse).show("fast");
			for (let j=0; j < d.appellations.length; j++) {
				let aoc = $('<input style="display:none" class="input-aoc" type="text" data-id="' + (j+1) + '" placeholder="Appellation ' + (j+1) + '\">');
				if (j==0) aoc.addClass('required');
				aoc.attr("value", d.appellations[j]);
				aoc.insertBefore($('#update .aoc .error')).show("fast");
			}
			$(this).attr('data-id', d._id);
			$('#update .submit').show("fast");
			$('#update .addAOC').show("fast");
			found = true;
			break;
		}
	}

	if (!found) {
		$('#update .submit').hide("fast");
		$('#update input:not("#input-domain-update"), #update .addAOC, #update .error').each(function () {
			$(this).hide("fast");
		})
		$('#update .input-aoc').each(function () {
			$(this).remove();
		});
	}
});

$('#update .submit').on("click", function () {
	let domaine = $('#update .input-domain').val().trim();
	let adresse = $('#update .input-address').val().trim();
	let aoc = [];

	$('#update .input-aoc').each(function (index) {
		let appellation = $('#update .input-aoc[data-id="' + (index + 1) + "\"").val().trim();
		if (appellation != "") {
			aoc.push(appellation);
		}
	});

	let formValid = true;
	$('#update .required').each(function () {
		if ($(this).val().trim() == '') {
			$(this).parent().find('.error').html('Ce champs est obligatoire.').show("fast");
			formValid = false;
		}
	});

	if (formValid) {
		if (!locked) {
			locked = true;

			$('#update .submit').addClass('disabled');

			$.ajax({
				url: GEOCODER_URL,
				method: 'GET',
				data: 'q=' + adresse.replace(/,/g, "+").replace(/ /g, "+") + '&limit=1',
				success: (data) => {
					if (data["features"].length != 0) {
						let longitude = data["features"][0]["geometry"]["coordinates"][0];
						let latitude = data["features"][0]["geometry"]["coordinates"][1];
						let domaineJSON = {
							"domaine": domaine,
							"adresse": adresse,
							"longitude": longitude,
							"latitude": latitude,
							"appellations": aoc
						}
						$.ajax({
							url: `${API_URL}domaine/` + $('#input-domain-update').attr('data-id'),
							method: "PUT",
							dataType: 'json',
							contentType: 'application/json',
							data: JSON.stringify(domaineJSON),
							success: function (data) {
								$('#update .result').html("Le domaine " + $('#input-domain-update').val() + " a bien été modifié.").css("color", "#28A745");
								getData();
							},
							error: function (data) {
								$('#update .result').html("Une erreur s'est produite. Veuillez réessayer.").css("color", "#DC3545");
							}
						});
					} else {
						$('#update .result').html("Adresse non reconnue. Veuillez réessayer.").css("color", "#DC3545");
					}
				},
				error: function (data) {
					$('#update .result').html("Une erreur s'est produite. Veuillez réessayer.").css("color", "#DC3545");
				}
			});

			setTimeout(function () {
				locked = false;
				$('#update .submit').removeClass('disabled');
			}, 1000);
		}
	}
});

$('#input-domain-delete').on("input", function () {
	let found = false;

	for (let i=0; i < listOfDomainForUpdate.length; i++) {
		d = listOfDomainForUpdate[i];
		d = listOfDomainForUpdate[i];
		if (d.domaine == $(this).val()) {
			$(this).attr('data-id', d._id);
			$('#delete .submit').show("fast");
			found = true;
			break;
		}
	}

	if (!found) {
		$('#delete .submit').hide("fast");
	}
});

$('#delete .submit').on("click", function () {
	if (!locked) {
		locked = true;
		$('#delete .submit').addClass('disabled');

		$.ajax({
			url: `${API_URL}domaine/` + $('#input-domain-delete').attr('data-id'),
			method: "delete",
			success: function (data) {
				$('#delete .result').html("Le domaine " + $('#input-domain-delete').val() + " a bien été supprimé.").css("color", "#28A745");
				$('#input-domain-delete').prop('value', '').trigger('input');
				getData();
			},
			error: function (data) {
				$('#delete .result').html("Une erreur s'est produite. Veuillez réessayer.").css("color", "#DC3545");
			}
		});

		setTimeout(function () {
			locked = false;
			$('#delete .submit').removeClass('disabled');
		}, 1000);
	}
});