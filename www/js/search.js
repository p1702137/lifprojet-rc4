//Retire les accents d'une chaine de caractère
function noAccents (s) {
	return s.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

//Vérifie si le mot clé k est contenu dans au moins l'une des aoc du domaine d
function checkInAoc (d, k) {
	for (let i = 0; i < d.appellations.length ;i++) {
		if(noAccents(d.appellations[i]).toUpperCase().includes(k)) return true;
	}
	return false;
}

//Au clic sur la croix, on supprim!e la saisie de l'utilisateur pour effacer les filtres
$('.search-sort img').on("click", () => {
	$('input').prop("value", "").trigger('input');
})

//A chaque lettre saisie par l'utilisateur dans la recherche
$('.search-sort input').on('input', () => {
	//On vide la liste des domaines
	$('.area').empty();

	//On déselectionne tous les icones de la map
	$('.selected')
		.removeClass('selected');
	
	//On récupère la chaine de mot clé, supprime les espaces, les accents et on la transforme en majuscule
	let keyWord = $.trim(noAccents($('input').val()).toUpperCase()).split(" ");

	//Si la recherche n'est pas vide
	if ($('input').val() != "") {
		//On affiche la croix permettant la suppression du filtre
		if($('.search-sort img').attr('src') != './img/croix-rouge.png'){
			$('.search-sort img').fadeOut(175, function() {
				$(this).attr('src', './img/croix-rouge.png');
				$(this).fadeIn(175);
			});
		}

		//Pour chaque domaine
		$.each(listOfDomain, (id, data) => {
			//On récupère ses infos et son icone (marker) sur la carte
			let d = data['domain'];
			let m = data['marker'];
			let found;

			//Pour chaque mot clé, s'il est contenu dans le nom ou l'adresse du domaine ou dans l'une des appellations qu'il propose
			for (let i = 0; i < keyWord.length; i++) {
				found = (noAccents(d.domaine).toUpperCase().includes(keyWord[i]) || noAccents(d.adresse).toUpperCase().includes(keyWord[i]) || checkInAoc(d, keyWord[i]));
				if (!found) break; //si au moins un mot de la recherche n'est pas contenu dans les infos du domaine, on ne l'affiche pas
			}
			//On l'affiche dans la liste des domaine et on affiche son icone sur la carte
			if (found) {
				display_domaine(d, m);
				m._icon.classList.remove('hidden');
			}
			//Sinon on en l'affiche pas et on cache son icone sur la carte
			else
				m._icon.classList.add('hidden');
		});
	}
	//Si la saisie est vide
	else{
		//On affiche tous les domaines dans la liste et toutes les icones sur la carte
		$.each(listOfDomain, (id, data) => {
			let d = data['domain'];
			let m = data['marker'];
			display_domaine(d, m);
			m._icon.classList.remove('hidden');
		});

		if($('.search-sort img').attr('src') != './img/loupe.png'){
			$('.search-sort img').fadeOut(175, function() {
				$(this).attr('src', './img/loupe.png');
				$(this).fadeIn(175);
			});
		}
	}
});

