const API_URL = 'http://212.195.183.176:1337/';

//Liste de couleurs pour les AOC
const COLORS = ['54A0FF', 'ED4C67', 'FFC312', 'B53471', '12CBC4', 'EE5A24', 'FDA7DF'];

var map = L.map('map').setView([46.5, 3], 6);

var listOfDomain = [];

var listMetsVins = {};

//Fonction qui affiche un domaine dans la liste des domaines avec ses infos et appellations ainsi que son icone sur la map
function display_domaine (domaine, marker) {
	let domain = $(`<div class='domaine'></div>`).on('click', () => {
		$('.selected').removeClass('selected');
		marker._icon.classList.add('selected');
	});

	let aoc = $(`<div class='list-aoc'></div>`);

	for (let i = 0; i < domaine.appellations.length; i++) {
		let nom = domaine.appellations[i];
		let div = $(`<div class="aoc"></div>`);
	let titre = $('<div class="header-aoc"><p style="background-color:#'+COLORS[i]+'">' + nom + '</p><img class="logo-mets" src="./img/fourchetteCouteau.png" alt = "mets" title="Quels plats ce vin peut-il accompagner ?"></div>');
		let listMets = $('<div class="list-mets"></div>');
		let ul = $('<ul></ul>');

		if(listMetsVins[nom] != undefined){
			listMets.append('Le vin <span style="color:#'+COLORS[i]+'">'+ nom +'</span> peut accompagner les plats suivants :');
			for(let j =0; j < listMetsVins[nom].length; j++){
				let li = $('<li><img src="./img/mets.png">' + listMetsVins[nom][j] +'</li>');
				ul.append(li);
			}
			listMets.append(ul);
		}
		else{
			listMets.append('Aucun plats n\'a été renseigné pour <span style="color:#'+COLORS[i]+'">'+ nom +'</span>.');
		}

		div.append(titre, listMets);
		div.find('.logo-mets').on("click", function(){
			div.find(".list-mets").toggle("fast");
		})
		aoc.append(div);
	}

	domain.append(location)
	.append($(`<div class='name'>${domaine.domaine}</div>`)) // name
	.append($(`<div class='address'>${domaine.adresse}</div>`)) // address
	.append(aoc);

	$('.area').append(domain);
}


// setup map
L.tileLayer('https://api.mapbox.com/styles/v1/{username}/{style_id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
	username: 'lifprojet-rc4',
	style_id: 'ck66oz8ta3pbt1ilfsndalxbs',
	minZoom: 5,
	maxZoom: 8,
	id: 'mapbox.streets',
	accessToken: 'pk.eyJ1IjoibGlmcHJvamV0LXJjNCIsImEiOiJjazY2b25kMmIwMDU2M2RzZDA0c3ZxYTNlIn0.BTXe1jClLLvh0wQtO5QV6w'
}).addTo(map);


$('.leaflet-control-attribution').remove(); // remove leaflet attribution
$('.leaflet-control-container').remove();   // remove leaflet base zoom

$('#zoom-in').click(function () {
	map.setZoom(map.getZoom() + 1);
	if ($('#zoom-out').hasClass('max')) $('#zoom-out').removeClass('max');
	if (map.getZoom() + 1 == map.getMaxZoom()) $('#zoom-in').addClass('max');
});

$('#zoom-out').click(() => {
	map.setZoom(map.getZoom() - 1);
	if ($('#zoom-in').hasClass('max')) $('#zoom-in').removeClass('max');
	if (map.getZoom() - 1 == map.getMinZoom()) $('#zoom-out').addClass('max');
});

//Récupération de la liste des mets en bdd
$.ajax({
	url: `${API_URL}mets`,
	method: 'GET',
	dataType: 'json',
	success : (data) => {
		for (let i = 0; i < data.length; i++) {
			let mv = data[i];
			if (mv.vin != undefined && mv.mets != undefined) {
				listMetsVins[mv.vin] =  mv.mets;
			}
		}

		//Lorsque les mets ont tous été récupéré, on peut récupérer la liste des domaines
		//On récupère les domaines après les mets, sinon la fonction display_domaine tentera d'accéder au tableau listeMetsVins avant qu'il soit remplit.
		$.ajax({
			url: `${API_URL}domaine`,
			method: 'GET',
			dataType: 'json',
			success : (data) => {
				for (let i = 0; i < data.length; i++) {
					let d = data[i];
					if(d.domaine != undefined && d.adresse != undefined && d.appellations != undefined && !isNaN(d.longitude) && !isNaN(d.latitude)){
						let m = new L.marker([d.latitude, d.longitude], { icon: new L.icon({ iconUrl: " ", iconSize: [30, 30], iconAnchor: [15, 30], popupAnchor: [0, -30] })});

						//Keep in memory the domain with the marker for researches
						listOfDomain.push({domain : d, marker : m});

						function selectionMarker () {
							$('.area').empty();
							$('.selected').removeClass('selected');
							m._icon.classList.add('selected');
							display_domaine(d, m);
						}
						m.on('click', selectionMarker).addTo(map);

						display_domaine(d, m);
					}
				}
			}
		});
	}
});




