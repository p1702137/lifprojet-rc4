crypt = 'b484ce38250186c2f9ac2c0fa85d5a0be7374dad80d1436964415ed97d72a0c3';
entry = prompt('Mot de passe :', '');

//Si l'utilisateur clique sur annuler, on revient à la page précédente

if (entry == null)
	window.history.back();

if (sha256(entry) == crypt)
	$('body').removeClass('hide');
else
	//Si le mdp de correspond pas, on recharge
	document.location.reload(true);