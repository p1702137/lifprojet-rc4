# Le site

> Les différentes fonctionnalités du site sont décrites dans le rapport.  
> Ce *readme* présente la structure du site et les outils utilisés

## Le préprocesseur SASS

L'unique outil de stylisation de page web, le [Css](https://developer.mozilla.org/fr/docs/Web/CSS), est loin d'être parfait. Au cours de gros projet, le  css devient très vite ingérable et inmaintenable. La majorité du temps, les développeurs utilisent des frameworks (comme [Bootstrap](https://getbootstrap.com/), [Bulma](https://bulma.io/) ou encore [TailWind Css](https://tailwindcss.com/)) qui leur permettent de réduire la quantité de code en utilisant des classes Css préfaites.  

Dans notre cas, le site n'est pas très grand. Utiliser un framework Css aurait comme unique conséquence d'alourdir le site et donc de le ralentir. Afin de palier néanmoins aux complications qu'engendre l'accumulation de Css, nous avons décidé d'utiliser un préprocesseur Css, un outil permettant de générer dynamiquement des fichiers Css.

[Sass](https://sass-lang.com/) est en quelques sortes un Css enrichi. Liste non exhaustive des fonctionnalités disponibles :
- variables
- fonctions
- [nesting](https://sass-lang.com/guide#topic-3)
- auto-préfixation (ajoute les lignes de code magiques pour que le css soit compatible sous n'importe quel navigateur)

Sass est très facile à prendre en main et nous à permis de réaliser un Css propre et structuré qui est en conséquence facilement maintenable.

## Les différentes librairies

### JQuery

[JQuery](https://jquery.com/) est l'une des librairies javascript la plus utilisé au monde, elle permet principalement de simplifier le développement de code javascript. Nous l'avons utilisé dans cette intérêt.

Simple exemple qui illustre à quel point jQuery peut être pratique (pas la peine de lire ça en détails) :
```js
/*
| Nous cherchons ici à ce que lorsque l'utilisateur clique sur un bouton,
| tous les 'h1' deviennent rouge
*/

// js natif
let btn = document.getElementById('button');
btn.addEventListener('click', function () {
	let arrayOfh1 = document.getElementsByTagName('h1');
	for (let i = 0; i < arrayOfh1.length; i++)
		arrayOfh1[i].style.color = "red";
});

// jquery
$('#button').click(function () {
	$('h1').css('color', 'red'));
});
```  

### Leaflet

[Leaflet](https://leafletjs.com/) est un librairie js de cartographie en ligne qui, à rapport à son concurrent google maps, est libre d'utilisation. Leaflet fourni avec ça carte plusieurs objets et méthodes comme des markers ou des fonctionnalités de tracé. Au sein de notre projet, nous avons utilisé les markers que nous avons personnalisé avec un svg libre de droit.