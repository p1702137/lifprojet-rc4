"""
| Script permettant de récupérer par scraping tous les accords mets et vins
| disponible sur le site https://www.platsnetvins.com/
|
| Le script utilise la librairie Beautiful Soup
| pip install bs4
"""

from urllib.request import urlopen as request
from bs4 import BeautifulSoup as soup
from time import sleep


# fonction nous donnant le contenu de la page mis en paramètre
def get (url) :
	client = request(url)
	content = client.read()
	client.close()
	return content


URL = 'https://www.platsnetvins.com/'
URL_DISHES = URL + 'aide-plats-mets.php?fbclid=IwAR2XPPnE5knKRUSr_Gfz-fAutwCkYtowkeKzH_Altt6LZoySXOOZIH2ZRtA'

dishes = dict ()
wines = dict()

for dish in (soup(get(URL_DISHES), 'html.parser')).findAll('a', {'class': 'ClsLiens'}) :
	dishes[dish.contents[0]] = dish['href']


for dish in dishes:
	url = URL + dishes[dish]
	print(url)

	for wine in (soup(get(url), 'html.parser')).findAll('a', {'class': 'Accord'}) :
		_wine =  wine.contents[0]

		if _wine not in wines:
			wines[_wine] = []

		wines[_wine].append(dish)
		print(_wine + " :+ " + dish)

	print("Page " + dish + " faite !")
	sleep(1) # sleep de 1 s


content = ""

for wine in wines :
	content += wine + " : "
	for dish in wines[wine] :
		content += dish + "  "
	content += "\n"


f = open("accords_mets_vins.txt","w+")
f.write(content)
f.close()