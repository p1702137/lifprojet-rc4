"""
| Script générant un fichier json à partir du csv `accords_gouv`
|
| Le script utilise la librairie excelrd :
| pip install excelrd
"""

import excelrd	# lecture d'un fichier excel	

file_name = ("accords.xlsx")
sheet = (excelrd.open_workbook(file_name)).sheet_by_index(0)

json = "["

# Pour chaque ligne du fichier excel
for r in range (1, sheet.nrows) :
	vin = "\n    \"vin\":\"" + sheet.cell_value(r,0).rstrip() + "\", "
	listeMets = sheet.cell_value(r,1).rstrip().split(';')
	mets = "\n    \"mets\":["

	for i in range (0, len(listeMets)) :
		mets += "\n        \"" + listeMets[i] + "\""
		mets += "\n    ]" if i == len(listeMets) - 1 else ","

	json += "\n  {  " + vin + mets + "\n  },"

# ouverture (et création s'il n'existe pas) du json en écriture
file = open("extraction_accords.json","w+")
file.write(json[:-1] + "\n]")
file.close()