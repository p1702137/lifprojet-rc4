# Extraction des données

## Récupération et complétion des données contenu dans l'Excel

Le fichier excel, contenant les données de notre cliente, n'est pas formalisé et est incomplet. La première étape de ce projet à donc été de récupérer et de compléter son contenu de manière automatisée à l'aide du script python `extraction_domaine.py`

## Récupération d'accords mets et vins pour enrichir nos données

### Tentative de Scraping

Le site [platsnetvins](https://www.platsnetvins.com/) contient un catalogue, plutôt complet, d'accords mets et vins. Cherchant à enrichir notre site (rep `www`), nous avons essayer de réaliser du scraping sur le site. Le script `scraping_accords_mets_vins.py` va dans un premier temps sur la [page](https://www.platsnetvins.com/aide-plats-mets.php?fbclid=IwAR2XPPnE5knKRUSr_Gfz-fAutwCkYtowkeKzH_Altt6LZoySXOOZIH2ZRtA), où se trouve la liste des mets, pour récupérer les mets et le lien vers leus pages dédiées. Ensuite on visite, avec une pause de 1 seconde (pour éviter que notre scraping soit trop facilement répérable), tous les liens. Sur ces pages, on y trouve les vins associés.
Une fois toutes ces informations collectées, il nous suffit de les réarranger dans un fichier json de façon à ce que chaque vin est un tableau de mets associé.

> Néanmoins le scraping sur la seconde partie n'est pas fonctionnel, nous supposons que le site (ou son hébergeur) à une solution anti-scraping.

###  Fichier csv de data.gouv

Le scraping n'étant pas au point, nous nous sommes rabbatu sur les données disponibles sur [data.gouv](https://www.data.gouv.fr/fr/). Le script `extraction_accords.py` génère à partir du fichier excel `accords` un fichier json qui pourra ensuite être utilisé pour alimenter la base de données NOSQL (Mongo Db).

## Tester les scripts

Chaque script contient en en-tête une courte description et la commande nécessaire à l'installation des dépendances.

> Nous utilisons [pip](https://pypi.org/project/pip/) comme "package installer".