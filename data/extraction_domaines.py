"""
| Script permettant de récupérer les données du fichier `withadresses.xlsx`
| et de les compléter et / ou corriger avec le geodecoder de data.gouv
|
| Le script fonctionne avec 2 librairies, l'installation passe par la commande :
| pip install excelrd requests
"""

import excelrd		# lecture d'un fichier excel
import requests		# requête http

# lecture du fichier contenant les domaines, leur adresse et les appellations
file_name = ("withadresses.xlsx")
sheet = (excelrd.open_workbook(file_name)).sheet_by_index(0)

json = "["

reussi = 0
total = 0

# Lien vers l'api geocoder du gouvernement
baseApiGeo = 'https://api-adresse.data.gouv.fr/search/?q='

# Pour chaque ligne du fichier excel
for r in range (sheet.nrows) :
	# Si le champs appellation est vide ou s'il contient le titre du champs, on considère que la ligne ne représente pas un domaine, on ne prend donc pas la ligne en compte
	if (sheet.cell_value(r, 2) != "" and sheet.cell_value(r, 2) != "APPELLATION 1"):
		total += 1

		# Si le champs adresse (champs 0) est vide, alors on recherchera par le nom de domaine (champs 1). Le nom de domaine est toujours présent dans le fichier excel contrairement à son adresse
		colonnePourGeoCode = 0 if sheet.cell_value(r,0).rstrip() != "" else 1

		# On supprime toutes les virgules et on remplace tous les espaces par des + afin de construire la requête http GET
		adressePourApi = sheet.cell_value(r,colonnePourGeoCode).rstrip().replace(",", "").replace(" ", "+")    

		# Exemple de requête : https://api-adresse.data.gouv.fr/search/?q=château+Lagrange+33250+SAINT+JULIEN+BEYCHEVELLE&postcode=33250
		geocode = requests.get(baseApiGeo+adressePourApi+"&limit=1")

		# HTTP code : 200 (réussite), 400 (introuvable), 500 (erreur)
		if geocode.status_code == 200 :
			geocode = geocode.json()
			longitude = "\n    \"longitude\":\""+str(geocode["features"][0]["geometry"]["coordinates"][0])+"\","
			latitude = "\n    \"latitude\":\""+str(geocode["features"][0]["geometry"]["coordinates"][1])+"\"," 
			reussi +=1
		elif geocode.status_code == 400 :
			print("Le domaine ", domaine, "n'a pas été trouvé'")
		else :
			print("Erreur")


		# Construction du domaine au format JSON pour remplissage de la base de données
		domaine = "\n    \"domaine\":\""+ sheet.cell_value(r,1).rstrip() + "\", "
		adresse = "\n    \"adresse\": \""+ sheet.cell_value(r,0).rstrip() + "\", "          
						
		appellations = "\n    \"appellations\":["

										
		# Pour chacun des champs appellation
		for c in range (2, sheet.ncols) :
			# S'il est rempli
			if sheet.cell_value(r,c) != "":
				appellations += "\n        \"" + sheet.cell_value(r,c).rstrip() + "\""
				if c != sheet.ncols-1 :
					if sheet.cell_value(r,c+1) != "" :
						appellations += ','
				else :
					appellations += '\n    ]'
										
		json += "\n  {  " + domaine + adresse + longitude + latitude + appellations + "\n  },"
		
json = json[:-1] + "\n]"

print("Total : " , total , "\nReussi : ", reussi, "\nPourcentage : ", (reussi/total)*100)

# ouverture (et création s'il n'existe pas) du json en écriture
file = open("extraction_excel.json","w+")
file.write(json)
file.close()