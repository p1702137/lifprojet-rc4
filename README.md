# Valorisation des données d'un commerçant de vin

```bash
git clone https://forge.univ-lyon1.fr/p1702137/lifprojet-rc4.git
```  

> **Matière** : LifProjet  
> **Sujet** : [RC4](http://perso.univ-lyon1.fr/fabien.rico/site/projet:2020:pri:sujet#rc4_valorisation_de_donnees_d_un_commercant_de_vin)   
> **Encadrant** : [Rémy Cazabet](mailto:remy.cazabet@univ-lyon1.fr)  
> **Groupe** : [Auriane Delahaie](mailto:auriane.delahaie@etu.univ-lyon1.fr) | [Alex Ligony](mailto:alex.ligony@etu.univ-lyon1.fr) | [Jérémy Thomas](mailto:jeremy.thomas@etu.univ-lyon1.fr)  