const mongoose = require('mongoose');

const DomaineSchema = mongoose.Schema({
	domaine: { type: String, require: true },
	adresse: { type: String, require: true },
	longitude: { type: String, require: true },
	latitude: { type: String, require: true },
	appellations: { type: [String], default: [] }
});

module.exports = mongoose.model('Domaine', DomaineSchema);