const mongoose = require('mongoose');

const MetsSchema = mongoose.Schema({
	vin: { type: String, require: true },
	mets: { type: [String], default: [] }
});

module.exports = mongoose.model('Mets', MetsSchema);