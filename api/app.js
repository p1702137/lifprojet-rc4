const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');

const PORT = 1337;
const URL_DB = process.env.DB_CONNECTION

app.use(bodyParser.json());


app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});


// Import Routes
const domaineRoute = require('./routes/domaines');
const metsRoute = require('./routes/mets');

app.use('/domaine', domaineRoute);
app.use('/mets', metsRoute);

// Connection to DB 
mongoose.connect(URL_DB, { useUnifiedTopology: true, useNewUrlParser: true }, () =>
	console.log('Connected to mongodb Atlas. The API is ready to use!')
);

app.listen(PORT);