# Application Programming Interface

L'Api sert d'intermédiaire entre les données stockées sur Mongodb atlas et notre site web. Ici nous utilisons [Express](https://expressjs.com/fr/), un framework NodeJs simple d'utilisation et très bien documenté.  
  
Liste des routes de l'api :  
- **POST** | `API_URL/domaine` | insert un domaine  
- **GET** | `API_URL/domaine`  | retourne l'ensemble des domaines  
- **PUT** | `API_URL/domaine/{id}` | met à jour le domaine mis en paramètre   
- **DELETE** | `API_URL/domaine/{id}` | supprime le domaine mis en paramètre  
- **GET** | `API_URL/mets` | retourne l'ensemble des accords mets et vins    

## Fonctionnement

Le fonctionnement de l'api passe uniquement par :
- le fichier `app.js` : il est en quelque sorte le noyau de l'application, on défini dedans quelques variables comme le port ou l'url de la base de données
- le répertoire `routes` contenant, comme son nom l'indique, les différentes routes de l'api.
- le répertoire `models` servant à décrire le schéma des données (types des attributs)

## Installation

#### Ajout du fichier `.env` contenant la chaîne de connection à Atlas MongoDb

```bash
echo DB_CONNECTION=mongodb+srv://Jeremy:rfpIfKVXfdk0xmJL@wine-czgaf.mongodb.net/Wine > .env
```

#### Installation des packages NodeJs

```bash
npm install
```

> Bien entendu, cette commande ne fonctionnera pas si [NodeJS](https://nodejs.org/en/download/) n'est pas installé sur votre machine

## Tester l'api en local

```bash
npm start
```

Rendez-vous à l'url [suivante](http://localhost:1337/domaine) dès que vous pouvez lire, dans votre terminal, le message :
```
Connected to mongodb Atlas. The API is ready to use !
```