const express = require('express');
const router = express.Router();
const Mets = require('../models/Mets');

router.get('/', async (req, res) => {
	try {
		const mets = await Mets.find();
		res.json(mets);
	} catch(err) {
		res.json({message:err});
	}
});

module.exports = router;