const express = require('express');
const router = express.Router();
const Domaine = require('../models/Domaine');

// Create
router.post('/', async (req, res) => {
	const domaine = new Domaine({
		domaine: req.body.domaine,
		adresse: req.body.adresse,
		longitude: req.body.longitude,
		latitude: req.body.latitude,
		appellations: req.body.appellations
	});

	try {
		const savedDomaine = await domaine.save();
		res.json(savedDomaine);
	} catch (err) {
		res.json({message:err});
	}
})

// Read all
router.get('/', async (req, res) => {
	try {
		const domaines = await Domaine.find();
		res.json(domaines);
	} catch(err) {
		res.json({message:err});
	}
});

// Update 
router.put("/:id", async (req, res) => {
	try {
		let domaine = await Domaine.findById(req.params.id).exec();
		domaine.set(req.body);
		let result = await domaine.save();
		res.json(result);
	} catch(err) {
		res.json({message:err});
	}
});

// Delete
router.delete("/:id", async (req, res) => {
	try {
		let result = await Domaine.deleteOne({ _id: req.params.id}).exec();
		res.json(result);
	} catch(err) {
		res.json({message:err});
	}
});


module.exports = router;